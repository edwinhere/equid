{-# LANGUAGE OverloadedStrings #-}
module BlockchainSpec where

import qualified Blockchain           as B
import           Control.Monad.Except
import           Control.Monad.State
import qualified Crypto.Hash          as C
import qualified Data.Binary          as Bi
import qualified Data.ByteString      as BS
import           Data.String.Conv
import           Data.Word
import           Debug.Trace
import           Test.Hspec
import           Text.Printf

spec :: Spec
spec =
  describe "Blockchain" $ do
    it "can hash block headers" $
      show (B.dhash block125552) `shouldBe` "1dbd981fe6985776b644b173a4d0385ddc1aa2a829688d1e0000000000000000"
    it "can convert bits to target" $ b2TE 0x18027e93 $ return "0000000000000000027e93000000000000000000000000000000000000000000"
    it "can convert bits to target" $ b2TE 0x1d00ffff $ return "00000000ffff0000000000000000000000000000000000000000000000000000"
    it "can convert bits to target" $ b2TE 0x1b0404cb $ return "00000000000404cb000000000000000000000000000000000000000000000000"
    it "can convert bits to target" $ b2TE 0x04000001 $ return "0000000000000000000000000000000000000000000000000000000000000100"
    it "can convert bits to target" $ b2TE 0x20000001 $ return "0000010000000000000000000000000000000000000000000000000000000000" -- this test case may be impossible according bitcoin difficulty adjustments
    it "can convert bits to target" $ b2TE 0x20ffffff $ return "7fffff0000000000000000000000000000000000000000000000000000000000" -- this test case may be impossible according bitcoin difficulty adjustments
    it "can mine block" $ testMine block125552 { B.blkNonce = 0x0, B.blkBits = 0x20000001 } -- the bits in the bitcoin blockchain is little endian, but in equid is big endian, so the correct nonce will be different too

testMine :: B.Block -> Expectation
testMine block = let
  (result, b) = runState (runExceptT B.mine) block
  in result `shouldBe` Right 0x2F562

b2TE :: Word32 -> Except String String -> Expectation
b2TE bits correctValue = let
      ours :: Except String String
      ours = hex <$> B.bitsToTarget bits
      in ours `shouldBe` correctValue

-- this test case is written in little endian so as to pass with data from the bitcoin blockchain
-- however the equid system is designed to use big endian throughout
-- https://blockexplorer.com/block/00000000000000001e8d6829a8a21adc5d38d0a473b144b6765798e61f98bd1d
block125552 :: B.Block
block125552 = B.Block {
    B.blkVersion = 0x01000000,
    B.blkPrevBlock = BS.pack . reverse $ [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0xa3, 0xa4, 0x1b, 0x85, 0xb8, 0xb2, 0x9a, 0xd4, 0x44, 0xde, 0xf2, 0x99, 0xfe, 0xe2, 0x17, 0x93, 0xcd, 0x8b, 0x9e, 0x56, 0x7e, 0xab, 0x02, 0xcd, 0x81],
    B.blkMerkleRoot = BS.pack . reverse $ [0x2b, 0x12, 0xfc, 0xf1, 0xb0, 0x92, 0x88, 0xfc, 0xaf, 0xf7, 0x97, 0xd7, 0x1e, 0x95, 0x0e, 0x71, 0xae, 0x42, 0xb9, 0x1e, 0x8b, 0xdb, 0x23, 0x04, 0x75, 0x8d, 0xfc, 0xff, 0xc2, 0xb6, 0x20, 0xe3],
    B.blkTimestamp = 0xc7f5d74d,
    B.blkBits = 0xf2b9441a,
    B.blkNonce = 0x42a14695
  }

hex :: BS.ByteString -> String
hex = concatMap (printf "%02x") . BS.unpack
