module Merkle where


import           Blockchain      (dhash)
import           Crypto.Hash
import qualified Data.ByteString as B

type Hash = Digest SHA256

joinHash :: Hash -> Hash -> Hash
joinHash a b = dhash (B.append a b)

merkleRoot :: [Hash] -> Hash
merkleRoot [h] = h
merkleRoot hs  = joinHash (merkleRoot left) (merkleRoot right) where
	(left, right) = splitAt i hs
	i = until (\x -> x*2 >= length hs) (*2) 1
