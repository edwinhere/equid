module Blockchain
    ( mine,
      hash,
      dhash,
      Block(..),
      Blockchain,
      bitsToTarget
    ) where

import           Control.Monad.Except
import           Control.Monad.State
import qualified Crypto.Hash          as C
import           Data.Bits
import qualified Data.ByteArray       as BA
import qualified Data.ByteString      as BS
import           Data.Word

data Block = Block {
    blkVersion    :: Word32,
    blkPrevBlock  :: BS.ByteString,
    blkMerkleRoot :: BS.ByteString,
    blkTimestamp  :: Word32,
    blkBits       :: Word32,
    blkNonce      :: Word32
  } deriving Show

type Blockchain = [Block]

mine :: ExceptT String (State Block) Word32
mine = do
  block <- get
  let
    target = fromOctets . BS.unpack <$> bitsToTarget (blkBits block)
    hash' = show $ dhash block
    hash'' = read ("0x" ++ hash') :: Integer
    nonce = blkNonce block
  test <- (hash'' <) <$> target
  if test
    then return nonce
    else do
      put $ block { blkNonce = nonce + 1 }
      mine


bitsToTarget :: (Monad m) => Word32 -> ExceptT String m BS.ByteString
bitsToTarget bits
  | bits > 0xffffffff = throwError "'bits' may not be larger than 4 bytes"
  | (\x -> x <= 3 || x > 32) (bits `shiftR` 24) = throwError "target exponent must be > 3 and < 32"
  | otherwise = let
      _exponent = fromIntegral $ bits `shiftR` 24
      _mantissa = bits .&. 0x007fffff
      _offset = 32 - _exponent
      _value :: [Word8]
      _value = octets $ _mantissa `shiftL` 8
      _word8s :: [Word8]
      _word8s = replicate _offset 0 ++ _value ++ replicate (32 - _offset - length _value) 0
      in return $ BS.pack _word8s

dhash :: Block -> C.Digest C.SHA256
dhash block = C.hash (hash block :: C.Digest C.SHA256)

hash :: (C.HashAlgorithm a) => Block -> C.Digest a
hash block = let
  toBS = BS.pack . octets
  _header = BS.concat [
      toBS . blkVersion $ block,
      blkPrevBlock block,
      blkMerkleRoot block,
      toBS . blkTimestamp $ block,
      toBS . blkBits $ block,
      toBS . blkNonce $ block
    ]
  in C.hash _header

octets :: Word32 -> [Word8]
octets w =
    [ fromIntegral (w `shiftR` 24)
    , fromIntegral (w `shiftR` 16)
    , fromIntegral (w `shiftR` 8)
    , fromIntegral w
    ]

fromOctets :: [Word8] -> Integer
fromOctets = foldl accum 0
  where
    accum a o = (a `shiftL` 8) .|. fromIntegral o
